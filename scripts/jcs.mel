/*global proc jcs()

    GUI for visualizing a Joint Coordinate system from two anatomical coordinate systems

    Written by David Baier
    last modified: 3/28/2020
*/
global proc jcs()
{
	if (`window -exists jcsWindow` == true) deleteUI jcsWindow;  

	int $height = 200;
	int $width = 500;

	string $mainWindow = `window -title "Joint Coordinate System" -menuBar true -maximizeButton false
		-minimizeButton true -sizeable true -resizeToFitChildren false jcsWindow`;
	columnLayout;
	frameLayout -label "Create Joint" -width $width -mw 5 -mh 5;
		columnLayout;			
			textFieldButtonGrp
				-label "Axes Name" -buttonLabel "Insert Selection" -eb 1
				-text "New name or existing axes" 
				-buttonCommand jcsaxesSelect jcsaxesFieldGroup;
				textFieldButtonGrp
				-label "Proximal Object" 
				-text "Select proximal and click Insert Selection" 
				-buttonLabel "Insert Selection" 
				-buttonCommand jcsproxSelect jcsproxFieldGroup;
			textFieldButtonGrp
				-label "Distal Object" 
				-text "Select distal and click Insert Selection" 
				-buttonLabel "Insert Selection" 
				-buttonCommand jcsdistSelect jcsdistFieldGroup;
			
			button	-label "Create Axes" -enable 1 -w ($width) -command makeJCSPivot jcsCreatePivot;
			button	-label "Close" -w ($width) jcscloseButton;
			button -edit -command "deleteUI jcsWindow" jcscloseButton;
			setParent ..;
		setParent ..;
	showWindow $mainWindow;
	window -e -width $width -height $height $mainWindow;
}

global proc jcsaxesSelect()
// for textbutton group  button for axes selection
{
	string $objs[] = `ls -sl`;
	if (size($objs) == 1)
		{textFieldButtonGrp -e -text $objs[0] jcsaxesFieldGroup;}
	else
		{confirmDialog -message "Must have 1 and only 1 object selected" -button "OK";}
}
global proc jcsproxSelect()
// for textbutton group  button for proximal element selection
{
	string $objs[] = `ls -sl`;
	if (size($objs) == 1)
		{textFieldButtonGrp -e -text $objs[0] jcsproxFieldGroup;}
	else
		{confirmDialog -message "Must have 1 and only 1 object selected" -button "OK";}
}
global proc jcsdistSelect()
// for textbutton group  button for distal element selection
{
	string $objs[] = `ls -sl`;
	if (size($objs) == 1)
		{textFieldButtonGrp -e -text $objs[0] jcsdistFieldGroup;}
	else
		{confirmDialog -message "Must have 1 and only 1 object selected" -button "OK";}
}

global proc makeJCSPivot()
{
	// get the user input and check for valid names
	string $axesName = `textFieldButtonGrp -q -text jcsaxesFieldGroup`;
	if (!`isValidObjectName $axesName`){error -n "invalid Axis name";}
	string $proxObj = `textFieldButtonGrp -q -text jcsproxFieldGroup`;
	if (!`objExists $proxObj`){error -n "proximal object does not exist";}
	string $distObj = `textFieldButtonGrp -q -text jcsdistFieldGroup`;
	if (!`objExists $distObj`){error -n "distal object does not exist";}
	
	// create an object that is a child of the proximal axes but is point and 
	// orient constrained to the distal object called axesNamedata
	string $jcsData = `group -em -n($axesName+"data")`;
	
	// make axes
	string $jcsAxes[] = `jcsMake($axesName+"_grp")`;
	string $grp = $jcsAxes[0];
	string $x = $jcsAxes[1];
	string $y = $jcsAxes[2];
	string $z = $jcsAxes[3];
	
	// parent them to proximal
	parent $jcsData $proxObj;
	//setAttr ($jcsData+".t") 0 0 0;
	//setAttr ($grp+".t") 0 0 0;
	
	// point constrain data node
	pointConstraint $distObj $jcsData;
	orientConstraint $distObj $jcsData;
	
	// connect x to xyz, y to yz and z to z rotations
	pointConstraint $distObj $grp;
	orientConstraint -offset 0 0 0 -weight 1 $proxObj $z;
	orientConstraint -offset 0 0 0 -weight 1 $proxObj $distObj $y;
	orientConstraint -offset 0 0 0 -weight 1 $distObj $x;
	//orientConstraint -offset 0 0 0 -weight 1 -skip x $distObj $y;
   // orientConstraint -offset 0 0 0 -weight 1 -skip x -skip y $distObj $z;
	
	// ensure nodes are unlock unlocked prior to parenting
//	string $atts[] = {".tx",".ty",".tz",".rx",".ry",".rz",".sx",".sy",".sz"};
//	for ($j in $jointGroup)
//		{for ($at in $atts){setAttr -lock false ($j+$at);}}
	

}

/**************************************************************************************
*****************  scripts for making the axes  ***************************************
**************************************************************************************/
global proc string[] jcsMake(string $name)
/*
A Group node with X,Y,Z axes underneath
*/
{
	// check for shcders
	jcsShaderMake();
	
	//make the 3 axes
	string $x = jcsAxisMake($name,"x");
	string $y = jcsAxisMake($name,"y");
	string $z = jcsAxisMake($name,"z");
	
	// group them
	string $outname = `group -n $name $x $y $z`;
	string $output[];
    $output[0] = $outname;
    $output[1] = $x;
    $output[2] = $y;
    $output[3] = $z;
    
    return $output;
    
}

global proc string jcsAxisMake(string $name, string $axis)
{
	jcsShaderMake();
	
	int $axisVec[]; 
	string $shader; string $ax;
	float $pivVec[];
	
	if ($axis == "x")
		{$axisVec = {1, 0, 0};
		$pivVec = {0.5, 0.0, 0.0};
		$ax = "X";
		$shader = "xShaderSG";}
	else if ($axis == "y")
		{$axisVec = {0, 1, 0};
		$pivVec = {0.0, 0.5, 0.0};
		$ax = "Y";
		$shader = "yShaderSG";}
	else if ($axis == "z")
		{$axisVec = {0, 0, 1};
		$pivVec = {0.0, 0.0, 0.5};
		$ax = "Z";
		$shader = "zShaderSG";}
	
	string $finalobj = $name+$ax;
    string $cylinder = $name+$ax+"axis";
    string $cone = $name+$ax+"arrow";

    polyCylinder -axis $axisVec[0] $axisVec[1] $axisVec[2] 
    -h 10 -r 0.1 -n $cylinder;
    sets -e -forceElement $shader;
    xform -piv $pivVec[0] $pivVec[1] $pivVec[2]  $cylinder;
    eval("move -a -"+$axis+" -0.5;");
    
    polyCone -n $cone -r 0.2 -h 1 -ax $axisVec[0] $axisVec[1] $axisVec[2];
    sets -e -forceElement $shader;
    xform -piv $pivVec[0] $pivVec[1] $pivVec[2] $cone;
    eval("move -a -"+$axis+" 5.0;");
    
    polyUnite -n ($finalobj) -ch 0 $cylinder $cone;
    return $finalobj;
}

global proc jcsShaderMake()
{
	/* checks for existance of shaders for axes and creates them
	if they don't already exist*/
	
	if(!`objExists "xShader"`)
    {
        shadingNode -asShader blinn -n "xShader";
        sets -renderable true -noSurfaceShader true -empty -name xShaderSG;
        connectAttr -f xShader.outColor xShaderSG.surfaceShader;
        setAttr "xShader.color" -type double3 1 0 0;
    }
    
    if(!`objExists "yShader"`)
    {
        shadingNode -asShader blinn -n "yShader";
        sets -renderable true -noSurfaceShader true -empty -name yShaderSG;
        connectAttr -f yShader.outColor yShaderSG.surfaceShader;
        setAttr "yShader.color" -type double3 0 1 0;
    }
    
    if(!`objExists "zShader"`)
    {
        shadingNode -asShader blinn -n "zShader";
        sets -renderable true -noSurfaceShader true -empty -name zShaderSG;
        connectAttr -f zShader.outColor zShaderSG.surfaceShader;            
        setAttr "zShader.color" -type double3 0 0 1;
    }
}

